library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.TYPES.all;

entity LCD is

   port (
      EN       : in  std_logic:='1';
      ADDR     : in  std_logic_vector (7 downto 0):="00000001";
      DATA     : in  std_logic_vector (7 downto 0):="00000001";
      -- LCD connection
      LCD_E    : out std_logic;
      LCD_RS   : out std_logic;
      LCD_RW   : out std_logic;
      SF_D     : out std_logic_vector (11 downto 8);
      CLK      : in  std_logic;                       -- 50 MHz
		ONE 		: out std_logic;  
      RESET    : in  std_logic

   );
end LCD;

architecture LCD_BODY of LCD is

signal DB          : std_logic_vector (7 downto 0);
signal COUNTER     : integer:=0;
signal COUNTER2     : integer:=0;
signal COUNTER3     : integer:=0;
signal DATA_PRENOS : std_logic:='1';
signal INIT_PRENOS  : std_logic:='0';
signal RESET_COUNTER2 :  std_logic:='0';
signal RESET_COUNTER1 :  std_logic:='0';
signal RESET_COUNTER3 :  std_logic:='0';
signal SF_D_I     : std_logic_vector (11 downto 8);
signal  SF_D_W     :  std_logic_vector (11 downto 8);
signal LCD_E_I    :  std_logic;
signal LCD_E_W   :   std_logic;
signal STAV_I       : T_STAV_I;

signal COUNTER3_ENABLE :  std_logic:='0';
signal DALSI_STAV_I : T_STAV_I;
signal STAV_W      : T_STAV_W;
signal DALSI_STAV_W : T_STAV_W;

begin


LCD_RW<='0';

MULTIPLEXER: process (INIT_PRENOS,LCD_E_W,SF_D_W,LCD_E_I,SF_D_I)
begin

if INIT_PRENOS='1' then 
	LCD_E<=LCD_E_W;
	SF_D<=SF_D_W;
else
	LCD_E<=LCD_E_I;
	SF_D<=SF_D_I;
end if;

end process;

COUNT2 : process(CLK, RESET_COUNTER2)
begin
			if CLK='1' and CLK'event then
				if RESET_COUNTER2='1' then 
					COUNTER2<=0;
				else
					COUNTER2<=COUNTER2+1;
				end if;
			end if;
		
end process;

COUNT3 : process(CLK, RESET_COUNTER3, COUNTER3_ENABLE)
begin
		
		
			if CLK='1' and CLK'event and COUNTER3_ENABLE='1'  then
				if RESET_COUNTER3='1' then 
					COUNTER3<=0;
				else
					COUNTER3<=COUNTER3+1;
				end if;
			end if;
		
end process;

COUNT1 : process(CLK, RESET_COUNTER1)
begin
		
		
			if CLK='1' and CLK'event then
					if RESET_COUNTER1='1' then 
						COUNTER<=0;
					else
						COUNTER<=COUNTER+1;
					end if;
			end if;
			
	
end process;
--#########################################################################################
--#########################################################################################
KLDS: process ( CLK ,STAV_I,COUNTER,DATA_PRENOS,EN,COUNTER3 )
		begin
			RESET_COUNTER1<='0';
			COUNTER3_ENABLE<='0';
			RESET_COUNTER3<='0';	
			case STAV_I is
				when START =>
					if COUNTER =  750000 then
						DALSI_STAV_I <= JEDNA;
						RESET_COUNTER1<='1';
					else 
						DALSI_STAV_I <= START;
						RESET_COUNTER1<='0';
					end if;
					
				when JEDNA =>
					if COUNTER =  11 then
						DALSI_STAV_I <= DVA;
						RESET_COUNTER1<='1';
					else 
						DALSI_STAV_I <= JEDNA;
						RESET_COUNTER1<='0';
					end if;	
					
				when DVA =>
					if COUNTER =  204999 then
						DALSI_STAV_I <= TRI;
						RESET_COUNTER1<='1';
					else 
						DALSI_STAV_I <= DVA;
						RESET_COUNTER1<='0';
					end if;	
			
				when TRI =>
					if COUNTER =  11 then
						DALSI_STAV_I <= CTYRI;
						RESET_COUNTER1<='1';
					else 
						DALSI_STAV_I <= TRI;
						RESET_COUNTER1<='0';
					end if;	
				
				when CTYRI =>
					if COUNTER =  4999 then
						DALSI_STAV_I <= PET;
						RESET_COUNTER1<='1';
					else 
						DALSI_STAV_I <= CTYRI;
						RESET_COUNTER1<='0';
					end if;	
				
				when PET =>
					if COUNTER =  11 then
						DALSI_STAV_I <= SEST;
						RESET_COUNTER1<='1';
					else 
						DALSI_STAV_I <= PET;
						RESET_COUNTER1<='0';
					end if;	
				
					when SEST =>
					if COUNTER =  1999 then
						DALSI_STAV_I <= SEDM;
						RESET_COUNTER1<='1';
					else 
						DALSI_STAV_I <= SEST;
						RESET_COUNTER1<='0';
					end if;	
					
					when SEDM =>
					if COUNTER =  11 then
						DALSI_STAV_I <= OSM;
						RESET_COUNTER1<='1';
					else 
						DALSI_STAV_I <= SEDM;
						RESET_COUNTER1<='0';
					end if;	
					
					when OSM =>
					if COUNTER = 2000 then
						DALSI_STAV_I <= FUNCTION_SET;
						RESET_COUNTER1<='1';
					else 
						DALSI_STAV_I <= OSM;
						RESET_COUNTER1<='0';
					end if;	
					
									
					
					when FUNCTION_SET =>
					if DATA_PRENOS =  '1' then
						DALSI_STAV_I <= ENTRY_MODE_SET;
						RESET_COUNTER1<='1';
						
					else 
						DALSI_STAV_I <= FUNCTION_SET;
						RESET_COUNTER1<='1';
					end if;					
					
					when ENTRY_MODE_SET =>
					if DATA_PRENOS =  '1' then
						DALSI_STAV_I <= DISP_ON;
						RESET_COUNTER1<='1';
					
					else 
						DALSI_STAV_I <= ENTRY_MODE_SET;
						RESET_COUNTER1<='1';
					end if;
					
					when DISP_ON =>
					if DATA_PRENOS =  '1' then
						DALSI_STAV_I <= WAIT_ON_EN;
						RESET_COUNTER1<='1';
						
					else 
						DALSI_STAV_I <= DISP_ON;
						RESET_COUNTER1<='1';
					end if;
					
					when WAIT_ON_EN => 
						RESET_COUNTER1<='1';
						if EN= '1' then
							DALSI_STAV_I <= CLEAR_DISP;
						else
							DALSI_STAV_I <= WAIT_ON_EN;
					
						end if;
							
					
					when CLEAR_DISP =>
					if DATA_PRENOS =  '1'  then
						DALSI_STAV_I <= CLEAR_DISP_WAIT;
						RESET_COUNTER1<='1';
							RESET_COUNTER3<='1';	
								COUNTER3_ENABLE<='1';
						
					else 
						DALSI_STAV_I <= CLEAR_DISP;
						if DATA_PRENOS =  '1' then
							RESET_COUNTER1<='1';
						end if;
					end if;
					
				when CLEAR_DISP_WAIT =>
					if COUNTER = 80000 then
						DALSI_STAV_I <= SET_ADDR1;
						RESET_COUNTER1<='1';
					else 
						DALSI_STAV_I <= CLEAR_DISP_WAIT;
						RESET_COUNTER1<='0';	
					end if;	
					
					
					
					when SET_ADDR1 =>
					
					if DATA_PRENOS =  '1' then
						DALSI_STAV_I <= SEND_DATA;
						RESET_COUNTER1<='1';
						RESET_COUNTER3<='1';
					
					else 
						DALSI_STAV_I <= SET_ADDR1;
						RESET_COUNTER1<='1';	
					end if;
					
					when SEND_DATA =>
					RESET_COUNTER1<='1';
					if DATA_PRENOS =  '1' and COUNTER3=7 then
						DALSI_STAV_I <= SET_ADDR2;
					
					
					else 
						if DATA_PRENOS =  '1' then
							COUNTER3_ENABLE<='1';
						end if;
						DALSI_STAV_I <= SEND_DATA;
					end if;
					
					when SET_ADDR2 =>
					RESET_COUNTER1<='1';
					if DATA_PRENOS =  '1' then
						DALSI_STAV_I <= SEND_ADDR;
						RESET_COUNTER3<='1';	
						COUNTER3_ENABLE<='1';
					
			
					else 
						DALSI_STAV_I <= SET_ADDR2;
						RESET_COUNTER3<='1';	
						COUNTER3_ENABLE<='1';
							
					end if;
					
					when SEND_ADDR =>
					RESET_COUNTER1<='1';
					if DATA_PRENOS =  '1' and COUNTER3=7 then
						DALSI_STAV_I <= WAIT_ON_EN;
						
						
					else 
						if DATA_PRENOS =  '1' then
							COUNTER3_ENABLE<='1';	
						end if;
						DALSI_STAV_I <= SEND_ADDR;
					end if;
	
					
				when others => DALSI_STAV_I<= START;
								RESET_COUNTER1<='1';
								
			end case;	
		end process KLDS;

--#########################################################################################
--#########################################################################################
	KLV : process ( STAV_I, COUNTER3 )
		begin 
		  LCD_RS<='0';
		  SF_D_I<="0000";
		   INIT_PRENOS<='0';
			case STAV_I is 
			
				when START => LCD_E_I<='0';
								  SF_D_I<="0000";
								  LCD_RS<='0';
								  DB <= "00000000";
								  INIT_PRENOS<='0';
								
				when JEDNA => LCD_E_I<='1';
								  SF_D_I<="0011";
								  DB <= "00000000";
								  INIT_PRENOS<='0';
				when DVA   => LCD_E_I<='0';
								  SF_D_I<="0000";
								  DB <= "00000000";
								  INIT_PRENOS<='0';
				when TRI   => LCD_E_I<='1';
								  SF_D_I<="0011";
								  DB <= "00000000";
								  INIT_PRENOS<='0';
				when CTYRI => LCD_E_I<='0';
								  SF_D_I<="0000";
								  DB <= "00000000";
								  INIT_PRENOS<='0';
				when PET   => LCD_E_I<='1';
								  SF_D_I<="0011";
								  DB <= "00000000";
								  INIT_PRENOS<='0';
				when SEST  => LCD_E_I<='0';
								  SF_D_I<="0000";
								  DB <= "00000000";
								  INIT_PRENOS<='0';
				when SEDM  => LCD_E_I<='1';
								  SF_D_I<="0010";
								  DB <= "00000000";
								  INIT_PRENOS<='0';
				when OSM   => LCD_E_I<='0';
								  SF_D_I<="0000";
								  DB <= "00000000";
								  INIT_PRENOS<='0';
				when FUNCTION_SET => DB<="00101000";
											LCD_RS<='0';
											INIT_PRENOS<='1';
											LCD_E_I<='0';
										
				when ENTRY_MODE_SET => DB <="00000110";
											LCD_RS<='0';
											INIT_PRENOS<='1';
											LCD_E_I<='0';
									
				when DISP_ON => DB <="00001100";
									LCD_RS<='0';
									LCD_E_I<='0';
									INIT_PRENOS<='1';
							
					when WAIT_ON_EN =>  DB <="00000000";
											LCD_RS<='0';
											INIT_PRENOS<='0';
											LCD_E_I<='0';
				when CLEAR_DISP => DB <="00000001";
									INIT_PRENOS<='1';
									LCD_RS<='0';
									LCD_E_I<='0';
								
				when CLEAR_DISP_WAIT => LCD_E_I<='0';
												SF_D_I<="0000";
												DB <= "00000000";
												INIT_PRENOS<='0';
								  
				when SET_ADDR1 => DB <="10000000";
									LCD_RS<='0';
									INIT_PRENOS<='1';
									LCD_E_I<='0';
									
								
								
										
				when SEND_DATA => 
							
				
									if DATA(7-COUNTER3) = '1' then  
										DB <= "00110001";
									else
										DB <= "00110000";
									end if;
									LCD_RS<='1';
									INIT_PRENOS<='1';
									LCD_E_I<='0';
				
				when SET_ADDR2 => DB <="11000000";
									LCD_RS<='0';
									INIT_PRENOS<='1';
									LCD_E_I<='0';
								
								
			
				when SEND_ADDR => 
								
									if ADDR(7-COUNTER3) = '1' then  
										DB <= "00110001";
									else
										DB <= "00110000";
									end if;
									LCD_RS<='1';
									INIT_PRENOS<='1';
									LCD_E_I<='0';
									
				when DISABLE_LCD => LCD_RS<='0';
									LCD_E_I<='0';
									SF_D_I<="0000";
									INIT_PRENOS<='0';
									DB <= "00000000";
				when others => LCD_RS<='0';
									LCD_E_I<='0';
									SF_D_I<="0000";
									DB <= "00000000";
			end case;
			
			


		
					
					
	end process KLV;
--#########################################################################################
--#########################################################################################

	KLV2: process ( STAV_W, DB )
	begin
	case STAV_W is
	
				when   START => 
									LCD_E_W<='0';
									SF_D_W<=DB(7 downto 4);
									DATA_PRENOS<='0';
					
				when   HORNI_P => 
									LCD_E_W<='0';
									SF_D_W<=DB(7 downto 4);
									DATA_PRENOS<='0';
					
				when   HORNI_V => 
									LCD_E_W<='1';
									SF_D_W<=DB(7 downto 4);
									DATA_PRENOS<='0';
				when   HORNI_VD =>
									LCD_E_W<='0';
									SF_D_W<=DB(7 downto 4);
									DATA_PRENOS<='0';
					
				when   HORNI_N =>
									LCD_E_W<='0';
									SF_D_W<="0000";
									DATA_PRENOS<='0';
				when   DOLNI_P =>
									LCD_E_W<='0';
									SF_D_W<=DB( 3 downto 0);
									DATA_PRENOS<='0';
				when   DOLNI_V =>
									LCD_E_W<='1';
									SF_D_W<=DB( 3 downto 0);
									DATA_PRENOS<='0';
				when   DOLNI_VD =>
									LCD_E_W<='0';
									SF_D_W<=DB( 3 downto 0);
									DATA_PRENOS<='0';
				when   DOLNI_N =>
									LCD_E_W<='0';
									SF_D_W<="0000";
									DATA_PRENOS<='0';

				when   CEKEJ => LCD_E_W<='0';
									SF_D_W<="0000";
									DATA_PRENOS<='1';
					
				when others => 
									LCD_E_W<='0';
									SF_D_W<="0000";
									DATA_PRENOS<='0';
			end case;

	
	
	end process KLV2;
--#########################################################################################
--#########################################################################################
	KLDS2: process ( CLK, STAV_W, INIT_PRENOS, COUNTER2 )
		begin
				case STAV_W is
				
				when  START => 
					RESET_COUNTER2<='1';
					if INIT_PRENOS =  '1'  then
						DALSI_STAV_W <= HORNI_P;
					else 
						DALSI_STAV_W <= START;
					end if;
				when   HORNI_P =>
					if COUNTER2 =  0 then
						DALSI_STAV_W <= HORNI_V;
						RESET_COUNTER2<='1';
					else 
						RESET_COUNTER2<='0';
						DALSI_STAV_W <= HORNI_P;
						
						
					end if;
				when   HORNI_V =>
					if COUNTER2 =  11  then
						DALSI_STAV_W <=  HORNI_VD;
						RESET_COUNTER2<='1';
					else 
						RESET_COUNTER2<='0';
						DALSI_STAV_W <= HORNI_V;
						
					end if;
				
				when   HORNI_VD => DALSI_STAV_W <= HORNI_N;
						RESET_COUNTER2<='1';
				
				when   HORNI_N =>
					if COUNTER2 =  49  then
						DALSI_STAV_W <= DOLNI_P;
						RESET_COUNTER2<='1';
					else 
					RESET_COUNTER2<='0';
						DALSI_STAV_W <= HORNI_N;
						
					end if;
				when   DOLNI_P =>
					if COUNTER2 =  1  then
						DALSI_STAV_W <= DOLNI_V;
						RESET_COUNTER2<='1';
					else 
						DALSI_STAV_W <= DOLNI_P;
						RESET_COUNTER2<='0';
						
					end if;
					
				when   DOLNI_V =>
					if COUNTER2 =  11  then
						DALSI_STAV_W <= DOLNI_VD;
						RESET_COUNTER2<='1';
					else 
						DALSI_STAV_W <= DOLNI_V;
						RESET_COUNTER2<='0';
						
					end if;
				when   DOLNI_VD => DALSI_STAV_W<=DOLNI_N;
						RESET_COUNTER2<='1';
				when   DOLNI_N =>
					if COUNTER2 =  1998 then
						DALSI_STAV_W <= CEKEJ;
						RESET_COUNTER2<='1';
					else 
						DALSI_STAV_W <= DOLNI_N;
						RESET_COUNTER2<='0';
					end if;					

				when   CEKEJ => DALSI_STAV_W<= START;
									RESET_COUNTER2<='1';
					
				when others => DALSI_STAV_W<= START;
								RESET_COUNTER2<='1';
			end case;	
		end process KLDS2;



--#########################################################################################
--#########################################################################################




REG : process (CLK)
	begin 
	if( CLK = '1' and CLK'event) then
		if RESET = '1' then
			STAV_I <= START;
		else
			STAV_I <= DALSI_STAV_I;
		end if;
	end if;
end process REG;	

REG2 : process (CLK)
	begin 
	if( CLK = '1' and CLK'event) then
		if RESET = '1' then
			STAV_W <= START;
		else
			STAV_W <= DALSI_STAV_W;
		end if;
	end if;
end process REG2;	


end LCD_BODY;
