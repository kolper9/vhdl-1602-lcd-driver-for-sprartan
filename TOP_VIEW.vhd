library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.TYPES.all;




entity TOP is
port(
		   ROTARY   : in  std_logic_vector (1 downto 0);  -- rotary button  (left, right)
     
      ADDR_O     : out std_logic_vector (7 downto 0):= (others => '0');  -- address register
		LCD_E    : out std_logic;
      LCD_RS   : out std_logic;
      LCD_RW   : out std_logic;
      SF_D     : out std_logic_vector (11 downto 8);
      COARSE   : in  std_logic;                      -- fine or coarse incrementation (rotary button push)
      CLK      : in  std_logic;                      -- 50 MHz
		ONE 		: out std_logic;  
      RESET    : in  std_logic
);		
end entity TOP; 

architecture TOP_BODY of TOP is




component ROTARY_HW is
   port (
      ROTARY   : in  std_logic_vector (1 downto 0);  -- rotary button  (left, right)
      EN       : out  std_logic;
      ADDR     : out std_logic_vector (7 downto 0):= (others => '0');  -- address register
      COARSE   : in  std_logic;                      -- fine or coarse incrementation (rotary button push)
      CLK      : in  std_logic;                      -- 50 MHz
      RESET    : in  std_logic
   );
end  component ROTARY_HW;

signal EN1     :  std_logic;
signal DATA     :  std_logic_vector (7 downto 0);
signal ADDR1     : std_logic_vector (7 downto 0):= (others => '0');  -- address register
signal dina :  std_logic_vector(7 downto 0):= (others => '0');
signal WEA : std_logic_vector(0 downto 0);

component BRAM
  port (
    clka : in std_logic;
    wea : in std_logic_vector(0 downto 0);
    addra : in std_logic_vector(7 downto 0);
    dina : in std_logic_vector(7 downto 0);
    douta : out std_logic_vector(7 downto 0)
  );
end component;
	
component LCD is

   port (
      EN       : in  std_logic;
      ADDR     : in  std_logic_vector (7 downto 0);
      DATA     : in  std_logic_vector (7 downto 0);
      -- LCD connection
      LCD_E    : out std_logic;
      LCD_RS   : out std_logic;
      LCD_RW   : out std_logic;
      SF_D     : out std_logic_vector (11 downto 8);
      CLK      : in  std_logic;                       -- 50 MHz
		ONE 		: out std_logic;  
      RESET    : in  std_logic
	
   );
end  component LCD;

begin

BRAM_INST : BRAM
port map (
		wea => wea,
      dina  => dina,
      addra     =>  ADDR1,
      douta   => DATA,
      clka       => CLK              -- 50 MHz
   
   );
ROTARY_HW_INST : ROTARY_HW 
   port map (
      ROTARY   => ROTARY,
      EN        => EN1,
      ADDR     =>  ADDR1,
      COARSE   => COARSE,
      CLK       => CLK,              -- 50 MHz
      RESET    => RESET
   );
	
--ONE<='1';
ADDR_O <= ADDR1;
wea<="0";
dina<=(others => '0');

LCD_INST : LCD port map (
		EN       => EN1,
      ADDR     => ADDR1,
      DATA     => DATA,
		ONE     => ONE ,
      LCD_E     => LCD_E,
      LCD_RS    => LCD_RS,
      LCD_RW   => LCD_RW, 
      SF_D     => SF_D,
      CLK     =>    CLK,               -- 50 MHz
      RESET    => RESET
		
	);

end architecture;