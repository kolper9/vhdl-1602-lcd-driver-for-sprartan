library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.all;
use ieee.std_logic_unsigned.all; 

entity ROTARY_HW is
   port (
      ROTARY   : in  std_logic_vector (1 downto 0);  -- rotary button  (left, right)
      EN       : out  std_logic;
      ADDR     : out std_logic_vector (7 downto 0):= (others => '0');  -- address register
      COARSE   : in  std_logic;                      -- fine or coarse incrementation (rotary button push)
      CLK      : in  std_logic;                      -- 50 MHz
      RESET    : in  std_logic
   );
end ROTARY_HW;

architecture ROTARY_BODY of ROTARY_HW is

	type T_STAV is ( START, RIGHT_A1, RIGHT_A2, RIGHT_A3, LEFT_B1, LEFT_B2, LEFT_B3);
	
	signal STAV			: T_STAV;
	signal DALSI_STAV	: T_STAV;
	
	signal ADDR_S : std_logic_vector (7 downto 0) := "00000000";
	signal INCR   : std_logic_vector (2 downto 0) := "000";

	signal ROTARY_TMP : std_logic_vector ( 1 downto 0 ) := "00";
	signal ROTARY_SF   : std_logic_vector ( 1 downto 0 ) := "00";
	signal COARSE_TMP : std_logic := '0';
	signal COARSE_S   : std_logic := '0';

begin

	D_ADDR : process ( CLK )
	begin
		if CLK = '1' and CLK'event then
			if RESET = '1' then
				ADDR_S <= "00000000";
			else 
				if INCR = "100" then -- -10
					ADDR_S <= ADDR_S - 10;
				elsif INCR = "101" then -- -1
					ADDR_S <= ADDR_S - 1;
				elsif INCR = "111" then -- +1
					ADDR_S <= ADDR_S + 1;
				elsif INCR = "110" then -- +10
					ADDR_S <= ADDR_S + 10;
				else
					ADDR_S <= ADDR_S;
				end if;
			end if;
		end if;
	end process D_ADDR;
	
	D_ADDR2 : process ( CLK )
	begin
		if CLK = '1' and CLK'event then
			if RESET = '1' then
				ADDR <= "00000000";
			else 
				ADDR <= ADDR_S;
			end if;
		end if;
	end process D_ADDR2;
	
	D_ROTARY_1 : process ( CLK )
	begin
		if CLK = '1' and CLK'event then
			if RESET = '1' then
				ROTARY_TMP <= "00";
			else 
				ROTARY_TMP <= ROTARY;
			end if;
		end if;
	end process D_ROTARY_1;
	
	D_ROTARY_2 : process ( CLK )
	begin
		if CLK = '1' and CLK'event then
			if RESET = '1' then
				ROTARY_SF <= "00";
			else 
				ROTARY_SF <= ROTARY_TMP;
			end if;
		end if;
	end process D_ROTARY_2;
	
	D_COARSE_1 : process ( CLK )
	begin
		if CLK = '1' and CLK'event then
			if RESET = '1' then
				COARSE_TMP <= '0';
			else 
				COARSE_TMP <= COARSE;
			end if;
		end if;
	end process D_COARSE_1;
	
	D_COARSE_2 : process ( CLK )
	begin
		if CLK = '1' and CLK'event then
			if RESET = '1' then
				COARSE_S <= '0';
			else 
				COARSE_S <= COARSE_TMP;
			end if;
		end if;
	end process D_COARSE_2;
	
	-- ==================================================
	
	KLV : process ( STAV, COARSE_S, ROTARY_SF )
	begin 
	INCR <= "000";
	EN <= '0';
		case STAV is
			when START 	  => NULL;
			when RIGHT_A1  => NULL;
			when RIGHT_A2  => NULL;
			when RIGHT_A3  =>
				if ROTARY_SF = "00" then
					EN <= '1';
					if COARSE_S = '1' then
						INCR <= "110";			-- +10
					else 
						INCR <= "111";			-- +1
					end if;
				end if;
			when LEFT_B1   => NULL;
			when LEFT_B2   => NULL;
			when LEFT_B3   =>
				if ROTARY_SF = "00" then
					EN<= '1';
					if COARSE_S = '1' then
						INCR <= "100";			-- -10
					else 
						INCR <= "101";			-- -1
					end if;
				end if;
		end case;
	end process KLV;
	
	KLDS : process ( STAV, ROTARY_SF ) 
	begin
		case STAV is
			when START 	  =>
				if ROTARY_SF = "01" then
					DALSI_STAV <= RIGHT_A1;
				elsif ROTARY_SF = "10" then
					DALSI_STAV <= LEFT_B1;
				else 
					DALSI_STAV <= START;
				end if;
			when RIGHT_A1  => 
				if ROTARY_SF(0) = '0' then
					DALSI_STAV <= START;
				elsif ROTARY_SF = "11" then
					DALSI_STAV <= RIGHT_A2;
				else 
					DALSI_STAV <= RIGHT_A1;
				end if;
			when RIGHT_A2  => 
				if ROTARY_SF = "00" then
					DALSI_STAV <= START;
				elsif ROTARY_SF = "01" then
					DALSI_STAV <= RIGHT_A1;
				elsif ROTARY_SF = "10" then
					DALSI_STAV <= RIGHT_A3;
				else 
					DALSI_STAV <= RIGHT_A2;
				end if;
			when RIGHT_A3  => 
				if ROTARY_SF = "01" then
					DALSI_STAV <= RIGHT_A1;
				elsif ROTARY_SF <= "11" then
					DALSI_STAV <= RIGHT_A2;
				elsif ROTARY_SF = "00" then
					DALSI_STAV <= START;
				else 
					DALSI_STAV <= RIGHT_A3;
				end if;
			when LEFT_B1   => 
				if ROTARY_SF(1) = '0' then
					DALSI_STAV <= START;
				elsif ROTARY_SF = "11" then
					DALSI_STAV <= LEFT_B2;
				else 
					DALSI_STAV <= LEFT_B1;
				end if;
			when LEFT_B2   => 
				if ROTARY_SF = "00" then
					DALSI_STAV <= START;
				elsif ROTARY_SF = "10" then
					DALSI_STAV <= LEFT_B1;
				elsif ROTARY_SF = "01" then
					DALSI_STAV <= LEFT_B3;
				else 
					DALSI_STAV <= LEFT_B2;
				end if;
			when LEFT_B3   => 
				if ROTARY_SF = "10" then
					DALSI_STAV <= LEFT_B1;
				elsif ROTARY_SF = "11" then  
					DALSI_STAV <= LEFT_B2;
				elsif ROTARY_SF = "00" then
					DALSI_STAV <= START;
				else 
					DALSI_STAV <= LEFT_B3;
				end if;
		end case;
	end process KLDS;
	
	REG_STAV : process ( CLK )
	begin
		if CLK = '1' and CLK'event then
			if RESET = '1' then
				STAV <= START;
			else 
				STAV <= DALSI_STAV;
			end if;
		end if;
	end process REG_STAV;
	
	
end ROTARY_BODY;